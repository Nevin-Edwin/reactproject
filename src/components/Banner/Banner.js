import React, { useState } from 'react'
import './Banner.css'
import axios from '../../axios'
import { apiKey, baseImg } from '../../Constants/Constant'
import { useEffect } from 'react'

function Banner() {
    const [movie, setMovie] = useState()
    useEffect(() => {
        let val = Math.floor(Math.random() * 19)
        axios.get(`/trending/all/day?api_key=${apiKey}`).then((response) => {
            setMovie(response.data.results[val])
        })
    }, [])

    return (
        <div
            style={{ backgroundImage: `url(${movie ? baseImg + movie.backdrop_path : ""})` }}
            className="banner">
            <div className="content">
                <h1 className="title">{movie ? movie.title : ""}</h1>
                <div className="bannerButtons">
                    <button className="button" id='button1'>Play</button>
                    <button className="button" id='button2'>My list</button>
                </div>
                <h1 className="description">{movie ? movie.overview : ""}</h1>
            </div>
            <div className="fade"></div>
        </div>
    )
}

export default Banner
