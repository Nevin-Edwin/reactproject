import React, { useEffect, useState } from 'react'
import "./RowPoster.css"
import axios from '../../axios'
import { apiKey, baseImg } from '../../Constants/Constant'
import Youtube from 'react-youtube'

function RowPoster(props) {
    const [orginals, setOrginals] = useState([])
    const [urlid, setUrlid] = useState('')
    useEffect(() => {
        axios.get(props.url).then((response) => {
            console.log(response.data);
            setOrginals(response.data.results)
        })

    }, [])
    const movieTrailer = (id) => {
        axios.get(`/movie/${id}/videos?api_key=${apiKey}&language=en-US`).then((response) => {
            if (response.data.results.length !== 0) {
                setUrlid(response.data.results[0])
            } else {
                console.log("Array EMpty");
            }
        })
    }
    // const opts = {
    //     height: '390',
    //     width: '100%',
    //     playerVars: {
    //         autoplay: 0,
    //     },
    // };

    return (
        <div className="row">
            <h2 className="subHead">{props.title}</h2>
            <div className="posters">
                {orginals.map((obj) =>

                    <img onClick={() => movieTrailer(obj.id)} className={props.isSmall ? "smallPoster pic" : "poster pic"} src={`${baseImg + obj.backdrop_path}`} alt="poster" />

                )}

            </div>
            {urlid && <Youtube videoId={urlid.key} />}
        </div>
    )
}

export default RowPoster
