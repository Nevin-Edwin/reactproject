import React from 'react'
import NavBar from './components/NavBar/NavBar';
import "./App.css"
import Banner from './components/Banner/Banner';
import RowPoster from './components/RowPoster/RowPoster';
import { actionUrl, comedyUrl, dramaUrl, horrorUrl, orginalsUrl } from './Constants/Constant'

function App() {
  return (
    <div className="App">
      <NavBar />
      <Banner />
      <RowPoster title="Netflix Orginals" url={orginalsUrl} />
      <RowPoster title="Action" isSmall url={actionUrl} />
      <RowPoster title="Comedy" isSmall url={comedyUrl} />
      <RowPoster title="Horror" isSmall url={horrorUrl} />
      <RowPoster title="Romance" isSmall url={dramaUrl} />

    </div>
  );
}

export default App;
